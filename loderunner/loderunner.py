# -*- coding: utf-8 -*-

import pygame

pygame.init()
x = 400
y = 30
velocidade = 10

fundo = pygame.image.load("selva1.png")
homem = pygame.image.load("homem.png")
grama = pygame.image.load("grama1.png")
quadrado = pygame.image.load("quadrado1.png")
escada1 = pygame.image.load("escada1.png")
#inimigo = pygame.image.load("inimigo.png")

janela = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Clone do Lode Runner")

janela_aberta = True
while janela_aberta:
	pygame.time.delay(50)
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			janela_aberta = False
			
	comandos = pygame.key.get_pressed()
	if comandos[pygame.K_UP]:
		y-= velocidade
	if comandos[pygame.K_DOWN]:
		y+= velocidade
	if comandos[pygame.K_RIGHT]:
		x+= velocidade
	if comandos[pygame.K_LEFT]:
		x-= velocidade
		
	janela.blit(fundo, (0, 0))
	janela.blit(homem, (x, 510))
	janela.blit(grama, (0, 550))
	janela.blit(quadrado, (500, 460))
	janela.blit(escada1, (460, 350))
	#janela.blit(inimigo,(490, 350))
	pygame.display.update()
	
pygame.quit() 
